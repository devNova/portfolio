# Portfolio

Hi, mein Name ist Patrick.

Willkommen auf meinem Portfolio! Hier stelle ich Ihnen meine Leidenschaft für die Softwareentwicklung vor und zeige
Ihnen meine Erfahrung und Fähigkeiten in verschiedenen Technologien und Programmiersprachen.

Als engagierter Entwickler bin ich stolz darauf, hier einige meiner Projekte zu präsentieren, die ein breites Spektrum
an Technologien und Frameworks umfassen.
Von Backend-Sprachen wie Kotlin, Java, und PHP über Frontend Frameworks wie Laravel, Vue.js, und weiteren bis hin zur
Entwicklung von Mobilen Applikationen mit Jetpack Compose.

## Projekte

* **Social Media Plattform**: [Laravel Social Media Plattform](https://gitlab.com/devNova/social-media-plattform)
    * Projekt zur Vertiefung des PHP Frameworks Laravel für das 3. Semester
    * genutzte Technologien:
        * PHP
        * Laravel
        * HTML/CSS
        * Bootstrap

* **Film Empfehlungsplattform**: [Movie Recommendation Plattform](https://gitlab.com/devNova/kotlinproject)
    * Aufbau einer Empfehlungsplattform mit Rest API, unter Verwendung der Open Source Datensätze von [movielens.org](https://grouplens.org/datasets/movielens/)
    * Ziel in diesem Projekt war es, mehrere Frameworks kennenzulernen, und zu konfigurieren
    * genutzte Technologien:
        * Kotlin
        * Exposed, Ktor, Dataframe, Compose for Desktop


* **Battleship**, ein Terminalbasiertes Spiel im Stil von Schiffe versenken: [Battleship](https://gitlab.com/tipa2/battleship)
    * Semesterabschluss-Projekt für das 2. Semester
    * Fokus auf einen möglichst effizienten Computergegner unter Anwendung verschiedener Algorithmen


* **Noted**, eine Notiz-App für Android Geräte: [Noted](https://gitlab.com/devNova/noteapp)
    * entstanden im Rahmen des Moduls Applikationsentwicklung als Projekt für das 2. Semester
    * umgesetzt mit Kotlin und Jetpack Compose
    * Persistence mit Room Database


* **Kursverwaltungsplattform**: [Verwaltungsplattform](https://gitlab.com/tipali1/fullstackproject)
    * Full-Stack Semesterabschluss-Projekt für das 1. Semester
    * genutzte Technologien:
        * Backend:
            * Java
            * Spring Boot, Hibernate, H2 Database
        * Frontend:
            * HTML/CSS/Javascript
            * Vue.js, Bootstrap

* **Hockey News**, ein Responsive Bootstrap Projekt: [Hockey News](https://gitlab.com/devNova/sports_news_responsive_bootstrap)
    * Webentwicklung - Zwischenprojekt für das 1. Semester 
    * Fokus auf Responsive Design
    * genutzte Technologien:
        * HTML/CSS:
        * Bootstrap:

***